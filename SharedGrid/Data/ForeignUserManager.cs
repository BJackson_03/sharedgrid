﻿namespace SharedGrid.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Media;
    using Annotations;
    using GalaSoft.MvvmLight.Messaging;
    using Messages;

    public class ForeignUserManager : INotifyPropertyChanged
    {
        private Dictionary<Guid, User> _foreignUsers;

        private Guid myId;

        private Dictionary<Guid, User> ForeignUsers
        {
            get
            {
                return this._foreignUsers;
            }

            set
            {
                if (value != this._foreignUsers)
                {
                    this._foreignUsers = value;
                    this.OnPropertyChanged(nameof(this.ForeignUsers));
                }
            }
        }

        public void UpdateUserLocation(Guid id, Point coord, DateTime timestamp)
        {
            if (!this.ForeignUsers.ContainsKey(id) &&
                id != this.myId)
            {
                this.LoginUser(id, "Anonymous");
            }

            if (this.ForeignUsers[id].MoveTimeStamp < timestamp)
            {
                this.ForeignUsers[id].Coord = coord;
                this.OnPropertyChanged(nameof(this.ForeignUsers));
            }
        }

        public string ForeignUsersPropertyName => nameof(this.ForeignUsers);

        public List<User> GetForeignUsersList()
        {
            return this.ForeignUsers.Select(foreignUser => foreignUser.Value).ToList();
        }

        /// <summary>
        /// Key = Brush
        /// <para>
        /// Value = Available
        /// </para> 
        /// </summary>
        private readonly Dictionary<SolidColorBrush, bool> availableColours;

        private readonly SolidColorBrush genericColour;

        public ForeignUserManager(Guid myId)
        {
            this.myId = myId;

            this.ForeignUsers = new Dictionary<Guid, User>();

            this.availableColours = new Dictionary<SolidColorBrush, bool>
            {
                { Brushes.Red, true },
                { Brushes.Yellow, true },
                { Brushes.Green, true },
                { Brushes.Indigo, true },
                { Brushes.Orange, true },
                { Brushes.Violet, true }
            };

            this.genericColour = Brushes.Black;

            Messenger.Default.Register<LoginMessage>(this, this.HandleLoginMessage);
            Messenger.Default.Register<LogoutMessage>(this, this.HandleLogoutMessage);

        }

        private void HandleLoginMessage(LoginMessage message)
        {
            Console.WriteLine($@"{message.Name} has logged in with ID: {message.Id}");
            this.LoginUser(message.Id, message.Name);
        }

        private void HandleLogoutMessage(LogoutMessage message)
        {
            if (this.ForeignUsers.ContainsKey(message.Id))
            {
                Console.WriteLine($@"{this.ForeignUsers[message.Id].Name} has logged out with ID: {message.Id}");
                this.LogoutUser(message.Id);
            }
        }

        private void LoginUser(Guid id, string name)
        {
            this.ForeignUsers.Add(
                id, 
                new User(id)
                {
                    Colour = this.GetNextColour(),
                    Coord = new Point(0,0),
                    Name = name
                });
            this.OnPropertyChanged(nameof(this.ForeignUsers));
        }

        private void LogoutUser(Guid id)
        {
            if (this.availableColours.ContainsKey(this.ForeignUsers[id].Colour))
            {
                this.availableColours[this.ForeignUsers[id].Colour] = true;
                this.ForeignUsers.Remove(id);
                this.OnPropertyChanged(nameof(this.ForeignUsers));
                this.InheritColour();
            }
        }

        private void InheritColour()
        {
            foreach (var foreignUser in this.ForeignUsers)
            {
                if (foreignUser.Value.Colour.Color == this.genericColour.Color)
                {
                    foreignUser.Value.Colour = this.GetNextColour();
                    if (Equals(foreignUser.Value.Colour, this.genericColour))
                    {
                        return;
                    }

                    this.OnPropertyChanged(nameof(this.ForeignUsers));
                }
            }
        }

        private SolidColorBrush GetNextColour()
        {
            foreach (var colour in this.availableColours)
            {
                if (colour.Value)
                {
                    this.availableColours[colour.Key] = false;
                    return colour.Key;
                }
            }

            return this.genericColour;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
