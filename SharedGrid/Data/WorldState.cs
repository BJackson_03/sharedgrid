﻿namespace SharedGrid.Data
{
    using System.Collections.Generic;

    public class WorldState
    {
        public List<List<string>> Cells { get; set; }

        public List<User> Users { get; set; }
    }
}
