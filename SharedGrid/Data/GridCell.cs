﻿namespace SharedGrid.Data
{
    using System;
    using System.Windows.Controls;

    public class GridCell : TextBox
    {
        public DateTime Timestamp { get; set; }

        public bool ForeignInfluence { get; set; }
    }
}
