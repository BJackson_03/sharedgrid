﻿namespace SharedGrid.Data
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    public class User
    {
        public SolidColorBrush Colour { get; set; }

        public Point Coord { get; set; }

        public Guid Id { get; }

        public string Name { get; set; }

        public DateTime MoveTimeStamp { get; set; }

        public User(Guid id)
        {
            this.Colour = new SolidColorBrush();
            this.Coord = new Point();
            this.Id = id;
            this.MoveTimeStamp = DateTime.MinValue;
        }
    }
}
