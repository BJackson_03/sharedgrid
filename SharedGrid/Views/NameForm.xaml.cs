﻿namespace SharedGrid.Views
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for NameForm.xaml
    /// </summary>
    public partial class NameForm : Window
    {
        public string Username { get; private set; }

        public NameForm()
        {
            this.InitializeComponent();
            this.TextBox.Focus();
        }

        private void StartBtn_OnClick(object sender, RoutedEventArgs e)
        {
            this.Username = this.TextBox.Text;
            this.Close();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.StartBtn_OnClick(sender, e);
            }
        }
    }
}
