﻿namespace SharedGrid.Views
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Shapes;
    using Data;
    using GalaSoft.MvvmLight.Messaging;
    using GridDataRepository;
    using Messages;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// First List = Row
        /// <para></para>
        /// Second List = Column
        /// </summary>
        private readonly List<List<GridCell>> cells;

        private readonly Rectangle mySelectRect;

        private readonly Guid myId;

        private readonly ForeignUserManager foreignUserManager;

        private readonly List<Rectangle> foreignSelectRects;

        private readonly List<Label> foreignSelectNameKey;

        private readonly List<Rectangle> foreignSelectColourKey;

        private readonly IGridDataRepository dataRepo;

        private User myUserProfile;

        public MainWindow()
        {
            this.InitializeComponent();

            this.KeyDown += this.Window_KeyDown;

            var nameForm = new NameForm();
            nameForm.ShowDialog();
            var username = nameForm.Username;
            if (!string.IsNullOrEmpty(username))
            {
                this.MyNameLabel.Content = $"{username} (me)";
                this.dataRepo = new AmazonSqsRepository();
                this.myId = Guid.NewGuid();
                this.cells = new List<List<GridCell>>();
                this.foreignSelectRects = new List<Rectangle>();
                this.foreignSelectColourKey = new List<Rectangle>();
                this.foreignSelectNameKey = new List<Label>();
                this.foreignUserManager = new ForeignUserManager(this.myId);
                this.foreignUserManager.PropertyChanged += this.ForeignUserManagerOnPropertyChanged;

                this.mySelectRect = new Rectangle
                {
                    Stroke = Brushes.Blue
                };

                this.RegisterForMessages();
                Task.Run(() => this.SetUpWorld(nameForm.Username));
            }
            else
            {
                this.Close();
            }
        }

        private void ForeignUserManagerOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == this.foreignUserManager.ForeignUsersPropertyName)
            {
                this.Dispatcher.Invoke(() =>
                {
                    var foreignUsers = this.foreignUserManager.GetForeignUsersList();

                    //// Ensure the correct number of Foreign Selection Rectangles are visible
                    while (foreignUsers.Count != this.foreignSelectRects.Count)
                    {
                        if (foreignUsers.Count > this.foreignSelectRects.Count)
                        {
                            var rect = new Rectangle();
                            this.foreignSelectRects.Add(rect);
                            this.MainGrid.Children.Add(rect);

                            var nameKey = new Label();
                            this.foreignSelectNameKey.Add(nameKey);
                            this.KeyGrid.Children.Add(nameKey);

                            var colourKey = new Rectangle
                            {
                                Height = 12.5,
                                Width = 12.5
                            };
                            this.foreignSelectColourKey.Add(colourKey);
                            this.KeyGrid.Children.Add(colourKey);

                            this.KeyGrid.RowDefinitions.Add(new RowDefinition());
                        }
                        else
                        {
                            this.MainGrid.Children.Remove(this.foreignSelectRects[0]);
                            this.foreignSelectRects.Remove(this.foreignSelectRects[0]);

                            this.KeyGrid.Children.Remove(this.foreignSelectColourKey[0]);
                            this.foreignSelectColourKey.Remove(this.foreignSelectColourKey[0]);

                            this.KeyGrid.Children.Remove(this.foreignSelectNameKey[0]);
                            this.foreignSelectNameKey.Remove(this.foreignSelectNameKey[0]);

                            this.KeyGrid.RowDefinitions.RemoveAt(0);
                        }
                    }

                    foreach (var cellRow in this.cells)
                    {
                        foreach (var cell in cellRow)
                        {
                            cell.ToolTip = null;
                        }
                    }

                    //// Set the Selection Rectange positions and colours
                    var index = 0;
                    foreach (var foreignUser in foreignUsers)
                    {
                        this.foreignSelectRects[index].Stroke = foreignUser.Colour;
                        Grid.SetRow(this.foreignSelectRects[index], (int) foreignUser.Coord.X);
                        Grid.SetColumn(this.foreignSelectRects[index], (int) foreignUser.Coord.Y);
                        this.cells[(int) foreignUser.Coord.X][(int) foreignUser.Coord.Y].ToolTip = foreignUser.Name;

                        this.foreignSelectColourKey[index].Fill = foreignUser.Colour;
                        Grid.SetRow(this.foreignSelectColourKey[index], index);
                        Grid.SetColumn(this.foreignSelectColourKey[index], 0);

                        this.foreignSelectNameKey[index].Content = foreignUser.Name;
                        Grid.SetRow(this.foreignSelectNameKey[index], index);
                        Grid.SetColumn(this.foreignSelectNameKey[index], 1);

                        index++;
                    }
                });
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (!this.myId.Equals(Guid.Empty))
            {
                Task.Run(() => this.dataRepo.Logout(this.myId));
            }
        }

        private void RegisterForMessages()
        {
            Messenger.Default.Register<MoveMessage>(this, this.HandleMoveMessage);
            Messenger.Default.Register<SetContentMessage>(this, this.HandleSetContentMessage);
        }

        private void HandleMoveMessage(MoveMessage message)
        {
            this.foreignUserManager.UpdateUserLocation(message.Id, message.Coord, message.Timestamp);
        }

        private void HandleSetContentMessage(SetContentMessage message)
        {
            if (this.cells[(int) message.Coord.X][(int) message.Coord.Y].Timestamp < message.Timestamp)
            {
                this.Dispatcher.Invoke(() =>
                {
                    this.cells[(int)message.Coord.X][(int)message.Coord.Y].ForeignInfluence = true;
                    this.cells[(int) message.Coord.X][(int) message.Coord.Y].Text = message.Value;
                });
                this.cells[(int) message.Coord.X][(int) message.Coord.Y].Timestamp = message.Timestamp;
                
                Console.WriteLine($@"User {message.Id} has set the contents of {message.Coord} to {message.Value}");
            }
        }

        private async Task SetUpWorld(string username)
        {
            try
            {
                this.myUserProfile = await this.dataRepo.Login(this.myId, username);
                var worldState = await this.dataRepo.GetWorldState(this.myId);

                this.Dispatcher.Invoke(() =>
                {
                    for (var col = 0; col < worldState.Cells[0].Count; col++)
                    {
                            this.MainGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    }

                    for (var row = 0; row < worldState.Cells.Count; row++)
                    {
                        this.MainGrid.RowDefinitions.Add(new RowDefinition());
                        this.cells.Add(new List<GridCell>());

                        for (var col = 0; col < worldState.Cells[0].Count; col++)
                        {
                            var newCell = new GridCell
                            {
                                HorizontalContentAlignment = HorizontalAlignment.Center,
                                VerticalContentAlignment = VerticalAlignment.Center,
                                Text = worldState.Cells[row][col],
                                Timestamp = DateTime.MinValue,
                                ForeignInfluence = false,
                            };

                            newCell.GotFocus += this.CellOnGotFocus;
                            newCell.TextChanged += this.CellOnTextChanged;
                            newCell.KeyDown += this.Window_KeyDown;
                           
                            Grid.SetRow(newCell, row);
                            Grid.SetColumn(newCell, col);

                            this.cells[row].Add(newCell);
                            this.MainGrid.Children.Add(newCell);
                        }
                    }

                    this.MainGrid.Children.Add(this.mySelectRect);
                });

                foreach (var user in worldState.Users)
                {
                    if (user.Id != this.myId)
                    {
                        Messenger.Default.Send(new LoginMessage(user.Name, user.Id));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($@"EXCEPTION: {e.Message}");
            }
        }

        private void CellOnTextChanged(object sender, TextChangedEventArgs e)
        {
            var source = e.Source as GridCell;
            for (var row = 0; row < this.MaxWidth; row++)
            {
                var col = this.cells[row].IndexOf(source);
                if (col != -1)
                {
                    if (!this.cells[row][col].ForeignInfluence)
                    {
                        this.dataRepo.SetVal(this.myId, new Point(row, col), source.Text);
                    }
                    else
                    {
                        this.cells[row][col].ForeignInfluence = false;
                    }

                    return;
                }
            }
        }

        private void CellOnGotFocus(object sender, RoutedEventArgs e)
        {
            for (var row = 0; row < this.MaxWidth; row++)
            {
                var col = this.cells[row].IndexOf(e.Source as GridCell);
                if (col != -1)
                {
                    Grid.SetRow(this.mySelectRect, row);
                    Grid.SetColumn(this.mySelectRect, col);
                    this.dataRepo.SetFocus(this.myId, new Point(row, col));
                    return;
                }
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Up && e.Key != Key.Right && e.Key != Key.Down && e.Key != Key.Left)
            {
                return;
            }

            for (var row = 0; row < this.cells.Count; row++)
            {
                for (var col = 0; col < this.cells[row].Count; col++)
                {
                    if (!this.cells[row][col].IsFocused)
                    {
                        continue;
                    }

                    switch (e.Key)
                    {
                        case Key.Up:
                            if (row > 0)
                            {
                                this.cells[row - 1][col].Focus();
                            }
                            break;

                        case Key.Right:
                            if (col > 0)
                            {
                                this.cells[row][col - 1].Focus();
                            }
                            break;

                        case Key.Down:
                            if (row < this.cells.Count - 1)
                            {
                                this.cells[row + 1][col].Focus();
                            }
                            break;

                        case Key.Left:
                            if (col < this.cells[row].Count - 1)
                            {
                                this.cells[row][col + 1].Focus();
                            }
                            break;
                    }

                    return;
                }
            }
        }
    }
}
