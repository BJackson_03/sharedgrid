﻿namespace SharedGrid.GridDataRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using Amazon.SQS;
    using Amazon.SQS.Model;
    using Data;
    using GalaSoft.MvvmLight.Messaging;
    using Messages;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class AmazonSqsRepository : IGridDataRepository
    {
        private AmazonSQSClient amazonSqsClient;

        private string myQueueUrl;
        private HttpClient httpClient;

        private const string ApiUrl = "https://20h5v9vzfk.execute-api.us-east-2.amazonaws.com";

        private bool shouldPoll;

        private Thread pollThread;
        private Guid myId;

        private void Poll()
        {
            while (this.shouldPoll)
            {
                var receiveRequest = new ReceiveMessageRequest
                {
                    QueueUrl = this.myQueueUrl,
                    MaxNumberOfMessages = 10,
                    VisibilityTimeout = 30,
                    WaitTimeSeconds = 20
                };

                var receiveResponse = this.amazonSqsClient.ReceiveMessage(receiveRequest);

                if (receiveResponse.Messages.Count != 0)
                {
                    foreach (var message in receiveResponse.Messages)
                    {
                        try
                        {
                            var bodyJson = JObject.Parse(message.Body);
                            var messageJson = JObject.Parse(bodyJson["Message"].Value<string>());

                            if (messageJson["UserId"].ToObject<Guid>() != this.myId)
                            {
                                this.HandleMessage(messageJson);
                            }

                            var deleteMessageRequest = new DeleteMessageRequest
                            {
                                QueueUrl = this.myQueueUrl,
                                ReceiptHandle = message.ReceiptHandle
                            };

                            var deleteResponse = this.amazonSqsClient.DeleteMessage(deleteMessageRequest);

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
            }
        }

        private void HandleMessage(JObject messageBody)
        {
            switch (messageBody["Action"].ToString())
            {
                case "Login":
                    Messenger.Default.Send(new LoginMessage(
                        messageBody["Payload"]["Name"].ToString(),
                        messageBody["UserId"].ToObject<Guid>()));
                    break;
                case "Logout":
                    Messenger.Default.Send(new LogoutMessage(
                        messageBody["UserId"].ToObject<Guid>()));
                    break;
                case "Move":
                    Messenger.Default.Send(new MoveMessage(
                        messageBody["UserId"].ToObject<Guid>(),
                        new Point(
                            messageBody["Payload"]["Coord"][0].ToObject<int>(),
                            messageBody["Payload"]["Coord"][1].ToObject<int>()),
                        messageBody["TimeStamp"].ToObject<DateTime>()));
                    break;
                case "SetContent":
                    Messenger.Default.Send(new SetContentMessage(
                            messageBody["UserId"].ToObject<Guid>(),
                        new Point(
                            messageBody["Payload"]["Coord"][0].ToObject<int>(),
                            messageBody["Payload"]["Coord"][1].ToObject<int>()),
                        messageBody["Payload"]["Value"].ToString(),
                        messageBody["TimeStamp"].ToObject<DateTime>()));
                    break;
            }
        }

        private void StartPolling()
        {
            this.shouldPoll = true;
            this.pollThread = new Thread(this.Poll);
            this.pollThread.Start();
        }

        private void StopPolling()
        {
            this.shouldPoll = false;
        }

        public async Task<User> Login(Guid id, string name)
        {
            this.myId = id;
            this.httpClient = new HttpClient
            {
                BaseAddress = new Uri(ApiUrl)
            };

            this.httpClient.DefaultRequestHeaders.Accept.Clear();
            this.httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var json = new JObject
            {
                { "UserId", id },
                { "Name", name },
                { "TimeStamp", DateTime.Now.ToUniversalTime() }
            };

            var response = await this.httpClient.PostAsync("/SharedGrid/login", new StringContent(json.ToString()));
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($@"Login Error: {response.StatusCode}");
                throw new Exception(response.StatusCode.ToString());
            }

            var responseContent = response.Content;
            var responseString = responseContent.ReadAsStringAsync().Result;
            var jsonResponse = JsonConvert.DeserializeObject<JObject>(responseString);

            var sqsConfig = new AmazonSQSConfig
            {
                ServiceURL = @"https://sqs.us-east-2.amazonaws.com"
            };

            this.amazonSqsClient = new AmazonSQSClient(sqsConfig);
            this.myQueueUrl = (string)jsonResponse["QueueUrl"];

            this.StartPolling();
            return new User(id);
        }

        public async Task Logout(Guid id)
        {
            this.StopPolling();
            var json = new JObject
            {
                { "UserId", id },
                { "TimeStamp", DateTime.Now.ToUniversalTime() }
            };

            var response = await this.httpClient.PostAsync("/SharedGrid/logout", new StringContent(json.ToString()));
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($@"Logout Error: {response.StatusCode}");
                throw new Exception(response.StatusCode.ToString());
            }
        }

        public async Task<WorldState> GetWorldState(Guid id)
        {
            var json = new JObject
            {
                { "UserId", id },
                { "DocumentId", 1 }
            };

            var response = await this.httpClient.PostAsync("SharedGrid/documentcontents", new StringContent(json.ToString()));
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($@"GetWorldState Error: {response.StatusCode}");
                throw new Exception(response.StatusCode.ToString());
            }

            var responseContent = response.Content;
            var responseString = responseContent.ReadAsStringAsync().Result;
            var jsonResponse = JsonConvert.DeserializeObject<JObject>(responseString);

            var cells = new List<List<string>>();

            for (var row = 0; row < (int) jsonResponse["Height"]; row++)
            {
                cells.Add(new List<string>());
                for (var col = 0; col < (int)jsonResponse["Width"]; col++)
                {
                    cells[row].Add(string.Empty);
                }
            }

            foreach (var item in jsonResponse["CellData"]["Items"])
            {
                var rowCol = (string)item["RowAndColumnIndex"]["S"];
                var rowOrCol = rowCol.Split('|');
                cells[int.Parse(rowOrCol[0])][int.Parse(rowOrCol[1])] = (string)item["Value"]["S"];
            }

            var users = jsonResponse["Users"]
                .Select(user => new User(new Guid((string) user["UserId"]["S"]))
                {
                    Name = (string) user["Name"]["S"]
                })
                .ToList();

            return new WorldState
            {
                Cells = cells,
                Users = users
            };
        }

        public async Task SetFocus(Guid id, Point focusCell)
        {
            var json = new JObject
            {
                { "UserId", id },
                { "TimeStamp", DateTime.Now.ToUniversalTime() },
                { "DocumentId", 1 },
                { "Coord", new JObject
                    {
                        { "row", focusCell.X },
                        { "col", focusCell.Y }
                    }
                }
            };

            await this.httpClient.PostAsync("/SharedGrid/move", new StringContent(json.ToString()));
        }

        public async Task SetVal(Guid id, Point cell, string val)
        {
            var json = new JObject
            {
                { "UserId", id },
                { "TimeStamp", DateTime.Now.ToUniversalTime() },
                { "DocumentId", 1 },
                { "Value", val },
                { "Coord", new JObject
                    {
                        { "row", cell.X },
                        { "col", cell.Y }
                    }
                }
            };

            await this.httpClient.PostAsync("/SharedGrid/setcontent", new StringContent(json.ToString()));
        }
    }
}
