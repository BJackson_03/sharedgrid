﻿namespace SharedGrid.GridDataRepository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Data;

    public class LocalGridDataRepository : IGridDataRepository
    {
        private const string FilePath = @"D:\GridData.txt";

        private static SolidColorBrush GetColorByCount(int index)
        {
            switch (index)
            {
                case 0:
                    return Brushes.Blue;
                case 1:
                    return Brushes.Red;
                case 2:
                    return Brushes.Yellow;
                case 3:
                    return Brushes.Green;
                case 4:
                    return Brushes.Purple;
                case 5:
                    return Brushes.Orange;
            }

            return Brushes.Black;
        }

        public Task<User> Login(Guid id, string name)
        {
            var lines = File.ReadAllLines(FilePath).ToList();

            var usersLineFound = false;
            var numUsers = 0;

            foreach (var line in lines)
            {
                if (usersLineFound)
                {
                    numUsers++;
                }

                if (line.Contains("Users"))
                {
                    usersLineFound = true;
                }
            }

            var newUser = new User(id);
            lines.Add($"{id}:{numUsers}:0,0");
            File.WriteAllLines(FilePath, lines.ToArray());

            return Task.FromResult(newUser);
        }

        public Task Logout(Guid id)
        {
            var lines = File.ReadAllLines(FilePath).ToList();

            var usersLineFound = false;
            for (var i = 0 ; i < lines.Count; i++)
            {
                if (usersLineFound)
                {
                    if (lines[i].Contains(id.ToString()))
                    {
                        lines.RemoveAt(i);
                        File.WriteAllLines(FilePath, lines.ToArray());
                        return null;
                    }
                }

                if (lines[i].Contains("Users"))
                {
                    usersLineFound = true;
                }
            }

            return null;
        }

        public Task<WorldState> GetWorldState(Guid id)
        {
            var lines = File.ReadAllLines(FilePath).ToList();

            var usersLineFound = false;
            var userLines = new List<string>();

            foreach (var line in lines)
            {
                if (usersLineFound)
                {
                    userLines.Add(line);
                }

                if (line.Contains("Users"))
                {
                    usersLineFound = true;
                }
            }

            var users = (from userLine in userLines
                select userLine.Split(':')
                into split
                let coord = split[2].Split(',')
                select new User(new Guid(split[0]))
                {
                    Coord = new Point(double.Parse(coord[0]), double.Parse(coord[1]))
                }).ToList();

            var cells = File.ReadAllLines(FilePath).ToList()
                .TakeWhile(line => !string.IsNullOrEmpty(line))
                .ToList()
                .Select(gridLine => gridLine
                    .Split(',')
                    .ToList())
                .ToList();

            return Task.FromResult(new WorldState
            {
                Cells = cells,
                Users = users
            });
        }

        public Task SetFocus(Guid id, Point focusCell)
        {
            var lines = File.ReadAllLines(FilePath).ToList();

            var usersLineFound = false;
            for (var i = 0; i < lines.Count; i++)
            {
                if (usersLineFound)
                {
                    if (lines[i].Contains(id.ToString()))
                    {
                        lines[i] = $"{id}:{lines[i][36]}:{focusCell.X},{focusCell.Y}";
                        File.WriteAllLines(FilePath, lines.ToArray());
                        return null;
                    }
                }

                if (lines[i].Contains("Users"))
                {
                    usersLineFound = true;
                }
            }

            return null;
        }

        public Task SetVal(Guid id, Point cell, string val)
        {
            var lines = File.ReadAllLines(FilePath).ToList();
            var gridLines = lines.TakeWhile(line => !string.IsNullOrEmpty(line)).ToList();

            var splitLine = gridLines[(int)cell.X].Split(',');
            splitLine[(int)cell.Y] = val;

            var comLine = splitLine.Aggregate(string.Empty, (current, split) => current + $"{split},");
            comLine.Remove(comLine.LastIndexOf(','));

            gridLines[(int)cell.X] = comLine;
            for (var i = 0; i < gridLines.Count; i++)
            {
                lines[i] = gridLines[i];
            }

            File.WriteAllLines(FilePath, lines.ToArray());
            return null;
        }
    }
}
