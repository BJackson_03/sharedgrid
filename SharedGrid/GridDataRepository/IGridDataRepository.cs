﻿namespace SharedGrid.GridDataRepository
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using Data;

    public interface IGridDataRepository
    {
        Task<User> Login(Guid id, string name);

        Task Logout(Guid id);

        Task SetFocus(Guid id, Point focusCell);

        Task SetVal(Guid id, Point cell, string val);

        Task<WorldState> GetWorldState(Guid id);
    }
}
