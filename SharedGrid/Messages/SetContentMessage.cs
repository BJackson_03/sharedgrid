﻿namespace SharedGrid.Messages
{
    using System;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;

    public class SetContentMessage : MessageBase
    {
        public Guid Id { get; }

        public Point Coord { get; }

        public string Value { get; }

        public DateTime Timestamp { get; }

        public SetContentMessage(Guid id, Point coord, string value, DateTime timestamp)
        {
            this.Id = id;
            this.Coord = coord;
            this.Value = value;
            this.Timestamp = timestamp;
        }
    }
}
