﻿namespace SharedGrid.Messages
{
    using System;
    using GalaSoft.MvvmLight.Messaging;

    public class LoginMessage : MessageBase
    {
        public string Name { get; }

        public Guid Id { get; }

        public LoginMessage(string name, Guid id)
        {
            this.Name = name;
            this.Id = id;
        }
    }
}
