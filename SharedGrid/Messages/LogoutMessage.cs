﻿namespace SharedGrid.Messages
{
    using System;
    using GalaSoft.MvvmLight.Messaging;

    public class LogoutMessage : MessageBase
    {
        public Guid Id { get; }

        public LogoutMessage(Guid id)
        {
            this.Id = id;
        }
    }
}
