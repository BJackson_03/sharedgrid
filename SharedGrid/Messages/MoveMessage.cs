﻿namespace SharedGrid.Messages
{
    using System;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;

    public class MoveMessage : MessageBase
    {
        public Guid Id { get; }

        public Point Coord { get; }

        public DateTime Timestamp { get; }

        public MoveMessage(Guid id, Point coord, DateTime timestamp)
        {
            this.Id = id;
            this.Coord = coord;
            this.Timestamp = timestamp;
        }
    }
}
